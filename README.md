### Create mirror repo ###

* git clone https://{REPOURL}/{REPONAME} --bare
* cd {REPONAME}
* git update-server-info
* mv hooks/post-update.sample hooks/post-update
* chmod a+x hooks/post-update
* run any http server, for eaxample :  http-server -p {PORT-NAME}  # should be installed (node install -g http-server)

### Run ###

* clone this repo
* node install -g nodemon
* npm install
* Create config.js file
* configure config using sample
* npm start