const CronJob = require('cron').CronJob;
const { updateRepo } = require('./helper');
const config = require('./config');


// Updating repo
updateRepo();
new CronJob('*/30 * * * *', function() {
    updateRepo();
}, null, true, 'America/Los_Angeles');
// --Updating repo




// Proxy
const http = require('http');
const httpProxy = require('http-proxy');
 
const proxy = httpProxy.createProxyServer({});
 
const server = http.createServer(function(req, res) {
    if (req.url && req.url.startsWith('/update')) {
        updateRepo();
    } else {
        proxy.web(req, res, { target: config.proxyTarget});
    }
});
 
console.log("listening on port 5050")
server.listen(5050);    
// --Proxy