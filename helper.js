const execSync = require('child_process').execSync;
const config = require('./config');
const { repoPath } = config;
const exec = (command) => {
    execSync(` cd ${repoPath} && ${command}`, options);
}
const options = {
    stdio: ['inherit', 'inherit', 'inherit']
};

module.exports = {
    updateRepo: () => {
        console.log('Updating master...');  
        exec('git fetch origin master:master');
        exec('git update-server-info');
        console.log("Master is ap to date.");
    }
}